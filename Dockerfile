FROM microsoft/dotnet:2.2-sdk-alpine AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY Demo.Web/*.csproj ./Demo.Web/
COPY Demo.Web.Tests/*.csproj ./Demo.Web.Tests/
RUN dotnet restore

# copy everything else and build app
COPY . ./
WORKDIR /app/Demo.Web
RUN dotnet publish -c Release -o out

# TODO figure out test step
FROM build AS testrunner
WORKDIR /app
ENTRYPOINT ["dotnet", "test", "--logger:trx"]

FROM microsoft/dotnet:2.2-aspnetcore-runtime-alpine AS runtime
WORKDIR /app
COPY --from=build /app/Demo.Web/out ./
ENV ASPNETCORE_URLS=http://+:5000
EXPOSE 5000
ENTRYPOINT ["dotnet", "Demo.Web.dll"]
