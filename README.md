# DevOps Demo

This is a simple .NET Core project used to demonstrate the use of GitLab's
AutoDevOps - we're using it to gain some quick experience in setting up
delivery pipelines.

# Attach a cluster

The GitLab docs are actually pretty good in retrospect. Once you create a cluster
on your preferred host, get the config file (so you can use kubectl), follow
the directions. Make sure you Base64-decode the token and ca-cert secrets.

Install Helm, Ingress, Prometheus.

Set REPLICAS or PRODUCTION_REPLICAS in Settings -> CI/CD -> Variables to scale.

You can attach multiple clusters as long as each is scoped to an environment (though
one can be scoped as a catch-all).

# Enable DevOps

You can use the Ingress IP and nip.io for the very first cut.

For a real domain, point a wildcard DNS entry to the Ingress IP. Install
cert-manager into the cluster and it will automatically provision certs for
all environments managed by the cluster.

# Setup build and test

Supplying a custom Dockerfile is sufficient for the build.  Slightly more complicated
for test as the default implementation assumes a herokuish buildpack with test support.

Building one should be straightforward, until then one can disable the test step for
experimenting with the rest of the devops pipeline.

Interesting factoid: automatically deploys postgres database by default.

Currently disable Test (until buildpack created) and Dependency Scanning (default
doesn't work for .NET Core) via Settings -> CI/CD -> Variables